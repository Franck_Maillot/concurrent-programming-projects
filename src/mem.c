/*****************************************************
 * Copyright Grégory Mounié 2008-2013                *
 * This code is distributed under the GLPv3 licence. *
 * Ce code est distribué sous la licence GPLv3+.     *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "mem.h"

/** squelette du TP allocateur memoire */

void *zone_memoire = 0;

//
typedef struct ListeBuddy
{
    struct ListeBuddy *Suiv;
} *Buddy;

// initialisation de la table des zones libres
// chaque case represente une puissance de 2: min 2^0 / max 2^20
Buddy tzl[21];

int 
mem_init()
{
  if (! zone_memoire)
    zone_memoire = (void *) malloc(ALLOC_MEM_SIZE);
  if (zone_memoire == 0)
    {
      perror("mem_init:");
      return -1;
    }
  
  // la zone memoire alloue est dans la case 2^19, on a deux buddies
  t[20] = zone_memoire;

  return 0;
}

void *
mem_alloc(unsigned long size)
{
  
  return 0;  
}

int 
mem_free(void *ptr, unsigned long size)
{
  /* ecrire votre code ici */
  return 0;
}


int
mem_destroy()
{
  /* ecrire votre code ici */

  free(zone_memoire);
  zone_memoire = 0;
  return 0;
}

